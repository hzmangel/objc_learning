#import <Foundation/Foundation.h>
#import "property.h"

int main (int argc, const char * argv[])
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    PropertyTester *property_tester = [[PropertyTester alloc] init];
    NSMutableString *tmp_str = [NSMutableString stringWithFormat:@"first"];

    property_tester.str_assign = tmp_str;
    property_tester.str_copy = tmp_str;
    property_tester.str_retain = tmp_str;

    NSLog (@"%p, %@", tmp_str, tmp_str);
    NSLog (@"%@", property_tester);

    [tmp_str appendString:@"second"];

    NSLog (@"%p, %@", tmp_str, tmp_str);
    NSLog (@"%@", property_tester);

    [pool drain];
    return 0;
}

