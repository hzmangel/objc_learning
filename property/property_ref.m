#import <Foundation/Foundation.h>
#import "property.h"

void print_ref_cnt(NSObject *obj1, NSObject *obj2, void (^block)(void)) {
    NSLog(@"====================");
    NSLog(@"Reference count of %@", obj1);
    NSLog(@"Before: obj1: %ld, obj2: %ld", (unsigned long)obj1.retainCount, (unsigned long)obj2.retainCount);
    block();
    NSLog(@"After : obj1: %ld, obj2: %ld", (unsigned long)obj1.retainCount, (unsigned long)obj2.retainCount);
    NSLog(@"@@@@@@@@@@@@@@@@@@@@");
}

int main (int argc, const char * argv[])
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    PropertyTester *property_tester = [[PropertyTester alloc] init];
    NSMutableString *tmp_str_assign = [NSMutableString stringWithFormat:@"assign"];
    NSMutableString *tmp_str_copy = [NSMutableString stringWithFormat:@"copy"];
    NSMutableString *tmp_str_retain = [NSMutableString stringWithFormat:@"retain"];

    NSLog(@"\n\n\nAssign");
    print_ref_cnt(tmp_str_assign, property_tester.str_assign, ^(void) { property_tester.str_assign = tmp_str_assign; });
    //print_ref_cnt(tmp_str, ^(void) { property_tester.str_assign.release; }); // Over release, seg fault

    NSLog(@"\n\n\nCopy");
    print_ref_cnt(tmp_str_copy, property_tester.str_copy, ^(void) { property_tester.str_copy = tmp_str_copy; });

    NSLog(@"\n\n\nRetain");
    print_ref_cnt(tmp_str_retain, property_tester.str_retain, ^(void) { property_tester.str_retain = tmp_str_retain; });

    NSLog (@"%@", property_tester);

    [pool drain];
    return 0;
}

