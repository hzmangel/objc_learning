#import <Foundation/Foundation.h>

@interface PropertyTester : NSObject

@property (assign) NSMutableString *str_assign;
@property (copy) NSMutableString *str_copy;
@property (retain) NSMutableString *str_retain;

@end


