#import "property.h"

@implementation PropertyTester

@synthesize str_assign;
@synthesize str_copy;
@synthesize str_retain;

- (NSString*)description
{
    return [NSString stringWithFormat:@"PropertyTester: %p\n\tassign: %p, %@ \n\tcopy: %p, %@ \n\tretain: %p, %@", self, str_assign, str_assign, str_copy, str_copy, str_retain, str_retain];
}

@end


